#!/bin/sh

rebuild () {
	echo "*******************************************************"
	for ELEMENT in utils logger babel-builder build core platform-browser platform-node simple
	do
		cd $ELEMENT

		npm unpublish --force --registry=http://localhost:4873/
		npm publish --force --registry=http://localhost:4873/

		cd ..
	done

	echo "*******************************************************"
	echo "Ready. Press <Enter> to rebuild. Or <Ctrl>-<C> to exit."
	read voidVar
	rebuild
};

rebuild
