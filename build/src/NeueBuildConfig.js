import Path from 'path';

export default class NeueBuildConfig {
	constructor(config) {
		if (typeof config !== 'object') {
			if (!config) return;
			throw new TypeError(`TypeError: NeueConfig: 'config' is not an \`object\`. (${typeof config})`);
		}

		const defaults = {
			Target: () => 'all',
			BuildContext: () => Path.dirname(module.parent.filename),
			TempOutputFolder: () => Path.join(this.BuildContext, './.tmp/'),
			DefaultModulePath: () => Path.join(this.BuildContext, './src/'),
			ClientScriptPath: () => Path.join(this.DefaultModulePath, './client.js'),
			ServerScriptPath: () => Path.join(this.DefaultModulePath, './server.js'),
			ResourceMapsFolder: () => Path.join(this.TempOutputFolder, './.maps/'),
			NodeSiteModulesPath: () => Path.join(this.BuildContext, './node_modules/'),
			MetaOutputFolder: () => Path.join(this.BuildContext, './.meta/'),
			BuildPublicOutputFolder: () => Path.join(this.BuildContext, './public/js/compiled/'),
			BuildServerOutputFolder: () => Path.join(this.BuildContext, './app/'),
			ResourceLoaderPath: () => Path.join(this.BuildContext, './nui/compiler/NeueResourceCompiler.js'),
			CleanPaths: () => ([
				this.TempOutputFolder,
				this.MetaOutputFolder,
				this.BuildServerOutputFolder,
				this.BuildPublicOutputFolder
			])
		};

		Object.assign(this, defaults, config);
		Object.keys(this).forEach((v) => {
			if (typeof this[v] === 'function') this[v] = this[v].apply(this);
		});
	}
}
