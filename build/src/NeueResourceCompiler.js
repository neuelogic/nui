const fs = require('fs');
const path = require('path');
const gutil = require('gulp-util');

const ResourceRef = require('nui-utils').ResourceRef;
const ValueTypeFilters = require('nui-utils').ValueTypeFilters;

function LogBuildMessage(msg, verboseOnly) {
	if (verboseOnly && !BUILD_ARGS.hasOwnProperty('verbose')) return;
	gutil.log('[webpack] [NeueLoader]', msg);
}

module.exports = function (source) {
	const file = this.request.split(/\!/).pop().split(/\?/)[0];
	let existsWithinModule = null;
	let resourceType = null;
	let resourceMeta = null;

	const booMods = NEUE_BUILD_OUTPUT_OBJECT.SiteModules;
	for (const mod of booMods) {
		if (existsWithin(mod.path, file)) {
			existsWithinModule = mod;

			for (var type of ['Actions', 'Pages', 'Views']) {
				if (existsWithin(
					`${path.join(mod.path, type.toLowerCase())}/`, file
				)) {
					resourceType = type;
					const resources = existsWithinModule.Types[type];
					for (const res of resources) {
						if (ValueTypeFilters.String(res)) continue;

						const resRef = res.ref;
						const fileLoc = path.join(res.path, res.name);

						if (fileLoc === file) {
							resourceMeta = res;
							break;
						}
					}

					break;
				}
			}

			break;
		}
	}

	/**
	 * @TODO: Will cachability conflict with our efforts to dissolve
	 * resources when they're not longer needed for the page??
	 */
	if (existsWithinModule && resourceType) {
		LogBuildMessage(`Loading NeueUI ${resourceType} Resource: \`${file}\``, true);
		LogBuildMessage(`Module exists within \`${existsWithin.ref}\` module`, true);
		this.cacheable();
	}

	if (resourceMeta) {
		LogBuildMessage('Resource has meta. Retrieving dependencies list.', true);
		for (var type of ['actions', 'pages', 'views']) {
			if (!resourceMeta.hasOwnProperty(type)) continue;

			LogBuildMessage(`Retrieving ${type} dependencies.`, true);
			for (const ref of resourceMeta[type]) {
				var modPath,
					fileName;
				if (ref.indexOf(path.sep) === -1) {
					modPath = existsWithinModule.path;
					fileName = `${ref}.js`;
				} else {
					const parts = ResourceRef(NEUE_BUILD_OUTPUT_OBJECT, ref);
					modPath = NEUE_BUILD_OUTPUT_OBJECT.SiteModules[parts[0]].path;
					fileName = `${parts[0]}.js`;
				}

				const referencedDependency = path.join(modPath, type, fileName);

				LogBuildMessage(`Including resource as dependency: \`${referencedDependency}\``, true);

				this.addDependency(referencedDependency);
				source = `require('${referencedDependency}');${source}`;
			}
		}
	}

	return source;
};

function existsWithin(modPath, file) {
	return (path.relative(modPath, file).indexOf('..') === -1);
}
