import React from 'react';
import ReactDOM from 'react-dom';

export default (NeueUI) => {
	NeueUI.Dispatcher.when('nui.router.renderable').then( PagePkg => {
		const Component = PagePkg.Component;
		const props = PagePkg.props || {};

		NeueUI.log('Rendering...');

		ReactDOM.render( <Component {...props} />, document.querySelector('[data-reactroot]').parentElement );
	});
}
