import vm from 'vm';
import fs from 'fs';
import path from 'path';
import { ResourceRef } from 'nui-utils';

const CacheMap = new WeakMap();

export default NeueUI => new (class NodeLoader {
	constructor() {
		NeueUI.Logger.attach(this, 'node-loader'); // Attach logger

		CacheMap.set(this, []);

		NeueUI.Dispatcher.when('nui.resource.import').then(this.handleImport.bind(this));
	}

	handleImport(ref) {
		const resRef = new ResourceRef(ref);

		const Cache = CacheMap.get(this);
		if (Cache.indexOf(String(resRef)) > -1) return;
		Cache.push(String(resRef));

		this.log(`Importing ${resRef}`, this.LOG_LOUD);

		const refPath = path.resolve(path.join(
			NeueUI.Config.appContext,
			`compiled.${resRef.serialize()}.js`
		));

		this.log([resRef.toString(), `Opening compiled resource package file: '${refPath}'`], this.LOG_DEBUG);

		fs.readFile(refPath, 'utf8', (err, src) => {
			if (err) {
				NeueUI.Dispatcher.resolve(`nui.resource.404.${String(resRef).replace(/[$:]/g, '.')}`, resRef);
				this.log(err);
				return;
			}

			this.log([resRef.toString(), 'Resource package file opened successfully'], this.LOG_DEBUG);

			const opts = {
				filename: refPath,
				displayErrors: true
			};

			const scrt = new vm.Script(src, opts);

			this.log([resRef.toString(), 'Executed script in new context'], this.LOG_DEBUG);

			const cntx = vm.createContext(Object.assign({
				NeueUI,
				module: { exports: {} },
				exports: {}
			}, global));

			const scriptInit = scrt.runInNewContext(cntx, opts);

			scriptInit(NeueUI);
		});
	}
})();
