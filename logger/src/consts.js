export const LOG_LVL = {
	QUIET: 0,
	LOUD: 1,
	VERBOSE: 2
};

export default function DefineLogLvlConsts(obj) {
	Object.defineProperties(obj, {
		LOG_QUIET: {
			enumerable: false,
			writable: false,
			configurable: false,
			value: LOG_LVL.QUIET
		},
		LOG_LOUD: {
			enumerable: false,
			writable: false,
			configurable: false,
			value: LOG_LVL.LOUD
		},
		LOG_DEBUG: {
			enumerable: false,
			writable: false,
			configurable: false,
			value: LOG_LVL.VERBOSE
		}
	});
}
