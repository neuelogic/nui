import NeueUI from 'nui';
import * as NeueBrowser from 'nui-platform-browser';

/* eslint-disable import/no-unresolved */
const Meta = require('../../../.nui/meta/meta.json');
/* eslint-enable import/no-unresolved */

// Get the current location.
const origPath = window.location.pathname;

// Uncomment the below if you want your nui page URLs to be recognized with .html
const reqPath = origPath; // .substr(0, origPath.indexOf('.html'));

const Nui = new NeueUI({
	appContext: '/',
	loggerVerbosity: NeueUI.Logger.LOG_DEBUG,
	Meta
});

Nui.use(NeueBrowser.Services);
Nui.use(NeueBrowser.History);
Nui.use(NeueBrowser.Loader);
Nui.use(NeueBrowser.Renderer);

Nui.Dispatcher.resolve('navigate', reqPath);
