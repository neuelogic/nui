import NeueUI from 'nui';
import * as NeueNode from 'nui-platform-node';

const { BUILD_ARGS } = global;

export default (req, res, Config) => {
	const appContext = Config.Build.BuildServerOutputFolder;

	/* eslint-disable import/no-unresolved */
	let PageStructure;
	try {
		PageStructure = require('../../../.nui/src/PageStructure.js').default;
	} catch (e) {
		PageStructure = require('./default/PageStructure.js').default;
	}
	const Meta = require('../../../.nui/meta/meta.json');
	/* eslint-enable import/no-unresolved */

	// Uncomment the below if you want your nui page URLs to be recognized with .html
	const reqPath = req.path.split('?')[0]; // .substr(0, req.path.indexOf('.html'));

	const Nui = new NeueUI({
		appContext,
		platform: NeueNode,
		loggerVerbosity: NeueUI.Logger[`LOG_${BUILD_ARGS.l.toUpperCase()}`],
		Meta
	});

	Nui.use(NeueNode.Services);
	Nui.use(NeueNode.Loader);

	if (reqPath.substr(0, 6) === '/rest/' && reqPath.length > 6) {
		Nui.Dispatcher.resolve('service', {
			path: reqPath, req, res, appContext
		});
	} else {
		Nui.use(PageStructure);

		Nui.use(NeueNode.Renderer).then(output => (
			(String(output.ref) === String(Nui.Router.getRoutes().NotFound)) ? res.status(404) : res
		).send(output.text)).catch((e) => {
			Nui.log(e);
			res.status(500).send('500 - Internal Server Error');
		});

		Nui.Dispatcher.resolve('navigate', reqPath);
	}
};
