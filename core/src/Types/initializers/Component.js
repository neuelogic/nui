const normalizeTypeName = typeName => typeName.substr(0, 1).toUpperCase() + typeName.substr(1).toLowerCase();

import { ResourceRef, ValueTypeFilters } from 'nui-utils';

export default NeueUI => class ComponentInitializer extends NeueUI.Initializers.Base {
	constructor(ResourcePkg) {
		super(ResourcePkg);

		this.ready = false;

		this.Component = ResourcePkg.component;

		if (!(this.Component.prototype instanceof NeueUI.Types.Component)) {
			return this.notifyReady(), void 0;
		}

		this.Dependencies = ResourcePkg.dependencies;
		this.initComponent();
	}

	notifyReady() {
		if (this.ready === true) return;
		clearTimeout(this.__readyTimeout);
		this.ready = true;
		NeueUI.Dispatcher.resolve(`nui.resource.ready.${String(this.ref).replace(/[$:]/g, '.')}`, this.getInterface());
	}

	initComponent() {
		const Component = this.Component;
		const Dependencies = this.Dependencies;

		Object.defineProperties(Component, {
			_views: {
				writable: false,
				configurable: true,
				enumerable: false,
				value: {}
			},
			_actions: {
				writable: false,
				configurable: true,
				enumerable: false,
				value: {}
			},
			_stores: {
				writable: false,
				configurable: true,
				enumerable: false,
				value: {}
			}
		});

		Object.defineProperties(Component.prototype, {
			Views: {
				writable: false,
				configurable: true,
				enumerable: false,
				value: ref => this.resourceGetter(ref, 'views')
			},
			Actions: {
				writable: false,
				configurable: true,
				enumerable: false,
				value: ref => this.resourceGetter(ref, 'actions')
			},
			Stores: {
				writable: false,
				configurable: true,
				enumerable: false,
				value: ref => this.resourceGetter(ref, 'stores').get()
			},

			devComponentWillUnmount: {
				writable: false,
				configurable: true,
				enumerable: false,
				value: Component.prototype.componentWillUnmount
			},
			componentWillUnmount: {
				writable: false,
				configurable: true,
				enumerable: false,
				value() {
					this._mounted = false;
					if (typeof this.devComponentWillUnmount === 'function') this.devComponentWillUnmount();
				}
			},
			devComponentDidMount: {
				writable: false,
				configurable: true,
				enumerable: false,
				value: Component.prototype.componentDidMount
			},
			componentDidMount: {
				writable: false,
				configurable: true,
				enumerable: false,
				value() {
					this._mounted = true;
					if (typeof this.devComponentDidMount === 'function') this.devComponentDidMount();
				}
			},

			devRender: {
				writable: false,
				configurable: true,
				enumerable: false,
				value: Component.prototype.render
			},
			render: {
				writable: false,
				configurable: true,
				enumerable: false,
				value: Component.prototype.nui_preRender
			}
		});

		if (
			Dependencies && Dependencies.All.length !== 0
		) {
			Object.defineProperties(Component, {
				nui_Deps: {
					writable: false,
					configurable: true,
					enumerable: false,
					value: Object.assign({}, Dependencies)
				},
				nui_DepsList: {
					writable: false,
					configurable: true,
					enumerable: false,
					value: this.getDepCompareList(Dependencies, this.ref)
				},
				nui_DepsLoaded: {
					writable: false,
					configurable: true,
					enumerable: false,
					value: []
				}
			});

			this.initWatchers();
		} else {
			Object.defineProperty(Component, 'nui_depsReady', {
				writable: false,
				configurable: true,
				enumerable: false,
				value: true
			});

			this.notifyReady();
		}
	}

	resourceGetter(relRef, type) {
		const resRef = this.ref.createRelative(relRef, type);
		this.log(`resourceGetter(${resRef})`, this.LOG_DEBUG);
		try {
			return this.Component[`_${resRef.type}`][resRef].Component;
		} catch (e) {
			this.log(['ERROR', `Failed to retrieve dependency: \`${resRef}\``]);
			try {
				if (!this.Dependencies) throw new Error('Make sure you\'re exporting your Assets.');
				else throw new Error('Try adding it to this component\'s `Assets`.');
			} catch (e) {
				this.log(['ERROR', e.stack || e.message]);
			}
			return (type === 'stores') ? { get: () => ({}) } : () => null;
		}
	}

	initWatchers() {
		this.__readyTimeout = setTimeout(
			this.notifyReady.bind(this),
			NeueUI.Config.defaultReadyTimeout
		);

		const deps = this.Dependencies;

		for (const type in deps) {
			for (const ref of deps[type]) {
				const relRef = this.ref.createRelative(ref, type);

				NeueUI.get(relRef).then(r => this.depLoaded(r));
			}
		}
	}

	depLoaded(resourceObj) {
		const ref = new ResourceRef(resourceObj.ref);
		this.Component.nui_DepsLoaded.push(ref);

		this.log([ref, 'dependency loaded'], this.LOG_DEBUG);

		Object.defineProperty(this.Component[`_${ref.type}`], ref, {
			writable: false,
			configurable: true,
			enumerable: true,
			value: resourceObj
		});

		const depsLoaded = this.Component.nui_DepsLoaded;
		const depsList = this.Component.nui_DepsList;

		if (this.compareArrs(depsLoaded, depsList)) {
			this.log([ref, 'was the last dependency to load'], this.LOG_DEBUG);

			Object.defineProperty(this.Component, 'nui_depsReady', {
				writable: false,
				configurable: true,
				enumerable: false,
				value: true
			});
			this.notifyReady();
		}
	}

	getDepCompareList(deps, parentRef) {
		const list = [];
		for (const type in deps) {
			for (const ref of deps[type]) {
				list.push(parentRef.createRelative(ref, type));
			}
		}
		return list;
	}
};
