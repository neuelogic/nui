const reserved = [
	'initialState',
	'isReady'
];

export default NeueUI => class StoreObjectWrapper extends NeueUI.Types.Store {
	constructor(obj) {
		super();

		this._obj = obj;

		this._isReady = false;

		this.generateWhens();
	}

	generateWhens() {
		const obj = this._obj;
		const keys = Object.keys(obj).filter(k => reserved.indexOf(k) < 0);

		const ret = keys.map((k) => {
			NeueUI.Dispatcher.when(k).then((payload) => {
				this.set(state => obj[k](state, payload));
			}).catch((e) => {
				if (obj.hasOwnProperty('catch')) obj.catch(e, this.get());
				else throw e;
			});
		});

		this._isReady = true;

		this.notify();
	}

	isReady() {
		const obj = this._obj;
		if (obj.isReady && typeof obj.isReady === 'function') {
			return obj.isReady(this.get());
		}
		return this._isReady;
	}

	initialState() {
		return this._obj.initialState;
	}
};
