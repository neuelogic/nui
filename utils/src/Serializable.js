export default class Serializable {
	toString() {
		let cache = [];
		const ret = JSON.stringify(this, (k, v) => ((typeof v !== 'object' && v !== null) ? ((cache.indexOf(v) !== -1) ? void 0 : cache.push(v), v) : v));
		cache = null; // Enable garbage collection
		return ret;
	}
}
